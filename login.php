<?php
include('login_auth.php'); // Includes Login Script
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Login Admin</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/login.css" rel="stylesheet">

</head>
<body>
    <div class="container">
        <div class="card card-container">
            <img id="profile-img" class="profile-img-card" src="img/bigtha-logo.png" />
            <p id="profile-name" class="profile-name-card"></p>
            <form acion="" method="post" class="form-signin" role="form" autocomplete="off">
                <input type="text" name="username" class="form-control" placeholder="Username" required autofocus>
                <input type="password" name="password" class="form-control" placeholder="Password" required>
                <button type="submit" name="submit" class="btn btn-lg btn-primary btn-block btn-signin" onclick="waitingDialog.show('Tunggu Sebentar...', {dialogSize: 'sm', progressType: 'success'});setTimeout(function () {waitingDialog.hide();}, 2000);"> Sign In </button>
            </form><!-- /form -->
        </div><!-- /card-container -->
    </div><!-- /container -->
                    <div class="col-xs-12" style="height:130px;">
                 <div class="container text-center">
            <p>Copyright &copy; PT. Bigtha Tryphena 2016</p>
            </div>
                </div>
        <script src="js/modal.js"></script>
        <script src="js/login.js"></script>
        </body>
</html>