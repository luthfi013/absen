<?php
include('session.php');
?>

<!DOCTYPE html>
<html lang="en">
    
    <!-- Header --> 
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Login Admin</title>

        <!-- Bootstrap Core CSS -->
        <link href="css/nav.css" rel="stylesheet">
        <link href="css/bs-doc.css" rel="stylesheet">
        <link href="css/bootstrap.css" rel="stylesheet">
        <link href="css/bootstrap.min.css" rel="stylesheet">
    </head>
    
    <!-- Navbar --> 
    <div id="custom-bootstrap-menu" class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container-fluid">
            <div class="navbar-header"><a class="navbar-brand">Dashboard Admin</a>
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-menubuilder"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse navbar-menubuilder">
            <ul class="nav navbar-nav navbar-left">
                <li><a href="/">Data Karyawan</a>
                </li>
                <li><a href="/products">Absensi</a>
                </li>
                <li><a href="/about-us">Penggajian</a>
                </li>
            </ul>

            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                Halo, <?php echo $login_session; ?>
                <b class="caret"></b>
                </a>
                <ul class="dropdown-menu">
                <li>
                <a class="text-right" href="logout_admin.php" >Log - Out</a>
                </li>
                </ul>
                </li>

            <li>
                <form class="navbar-form" role="search">
                    <div class="input-group">
                    <input class="form-control" type="text" name="q" placeholder="Cari...">
                    <div class="input-group-btn">
                    <button class="btn btn-default" type="submit">
                    <i class="glyphicon glyphicon-search"></i>
                    </button>
                    </div>
                    </div>
                </form>
            </li>

            </ul>
        </div>
        </div>
    </div>

    <!-- Body --> 
    <body>
<div class="container" id="body-content">

            <div class="row">

                <div class="col-md-2"></div>

                <div class="col-xs-11 col-md-8">        
                        <br>
                        <h1>Getting started</h1>

                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean rhoncus, nisi condimentum pretium ultricies, massa sem porttitor ligula, venenatis luctus felis lorem eget sapien. Nulla nec lectus tristique nunc eleifend interdum pellentesque non nunc. Donec justo justo, tempor eget mattis in, fermentum vitae metus. Nulla quis metus sed erat vulputate fermentum. Nunc pharetra leo non mollis iaculis. Etiam egestas urna sed fermentum ornare. Nulla facilisi. Vestibulum sagittis magna ut tempor pulvinar. Etiam accumsan lectus ornare libero iaculis cursus. Ut imperdiet sollicitudin massa in posuere. In vel mi lacus. In hac habitasse platea dictumst. Morbi ultrices turpis non molestie ornare.</p>
                    
                    <h1>Font awesome</h1>

                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean rhoncus, nisi condimentum pretium ultricies, massa sem porttitor ligula, venenatis luctus felis lorem eget sapien. Nulla nec lectus tristique nunc eleifend interdum pellentesque non nunc. Donec justo justo, tempor eget mattis in, fermentum vitae metus. Nulla quis metus sed erat vulputate fermentum. Nunc pharetra leo non mollis iaculis. Etiam egestas urna sed fermentum ornare. Nulla facilisi. Vestibulum sagittis magna ut tempor pulvinar.</p>
                    
                    <h1>Bootstrap</h1>

                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean rhoncus, nisi condimentum pretium ultricies, massa sem porttitor ligula, venenatis luctus felis lorem eget sapien. Nulla nec lectus tristique nunc eleifend interdum pellentesque non nunc. Donec justo justo, tempor eget mattis in, fermentum vitae metus. Nulla quis metus sed erat vulputate fermentum. Nunc pharetra leo non mollis iaculis. Etiam egestas urna sed fermentum ornare. Nulla facilisi. Vestibulum sagittis magna ut tempor pulvinar. Donec justo justo, tempor eget mattis in, fermentum vitae metus. Nulla quis metus sed erat vulputate fermentum. Nunc pharetra leo non mollis iaculis. Etiam egestas urna sed fermentum ornare. Nulla facilisi. Vestibulum sagittis magna ut tempor pulvinar.

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque tempus id ipsum et condimentum. Quisque eu dignissim augue. Nullam mattis velit eleifend, luctus diam nec, tincidunt leo. Nunc posuere dignissim enim, id scelerisque leo. Integer id nibh nec massa adipiscing ullamcorper. Curabitur gravida posuere faucibus. Cras rhoncus nunc vel diam posuere egestas. Nunc nunc nisl, facilisis id ante vitae, euismod ultricies arcu. Sed euismod orci magna, at lacinia sem blandit ac. Duis nec faucibus purus. Vestibulum lacus tortor, adipiscing at egestas nec, vehicula nec turpis. Donec et libero a mi venenatis blandit. Pellentesque id laoreet urna. Aliquam augue nulla, cursus vitae tortor in, sodales luctus erat.

Suspendisse a euismod enim, at mollis eros. Phasellus aliquet lacus eget risus adipiscing iaculis. Duis facilisis placerat purus. Vivamus congue libero sed elit laoreet, sed venenatis tellus suscipit. Sed ut velit metus. Vivamus in accumsan neque. Duis in egestas nulla.

Suspendisse potenti. Vivamus iaculis commodo tellus et faucibus. Proin pulvinar lobortis tempus. Sed vitae volutpat diam, at porttitor nulla. Proin in turpis iaculis, eleifend mi at, vehicula leo. Donec commodo eget enim ut consectetur. Nunc sit amet accumsan orci. Nunc nisi metus, fermentum ac risus in, euismod interdum orci. Nullam eleifend mauris in orci porta convallis.

Donec augue felis, feugiat nec aliquet in, pharetra sit amet urna. Phasellus ut lacus sed metus pellentesque aliquam non vitae erat. Etiam quis velit sem. Praesent ullamcorper euismod dignissim. Mauris magna mauris, pellentesque at lorem mattis, molestie aliquet sem. Maecenas pellentesque massa odio, in cursus lectus adipiscing a. Aenean sit amet lacus risus. Duis accumsan sem quis erat fringilla scelerisque. Cras nisl sem, sagittis nec luctus eget, volutpat eu eros. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;

Quisque dictum pulvinar risus, ac bibendum nulla pellentesque a. Duis vitae arcu ut neque feugiat mattis. Nam sit amet tellus ac purus consequat bibendum nec nec libero. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nunc quis nulla fringilla, congue ipsum eget, commodo neque. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Etiam id feugiat dolor. Vestibulum a mauris vitae sem facilisis blandit at a nisl. Sed ut ipsum a nisl dignissim volutpat eget ac urna. Ut metus mauris, aliquam vitae neque vel, pretium tincidunt neque. Mauris vehicula lacinia sapien. Ut ullamcorper ante velit, sed posuere nunc tempor quis.

                        </p>
                    
                </div>
                
                <div class="col-md-2">
                    <nav>
                        <ul class="nav nav-stacked bs-docs-sidenav" data-spy="affix" data-offset-top="365">
                          <li>
                            <a href="#">Getting Started</a>
                            <ul class="nav nav-stacked">
                                <li><a href="#GroupASub1">Sub-Group 1</a></li>
                                <li><a href="#GroupASub2">Sub-Group 2</a></li>
                            </ul>
                          </li>
                          <li><a href="#gridSystem"><i class="icon-chevron-right"></i> Grid system</a></li>
                          <li><a href="#fluidGridSystem"><i class="icon-chevron-right"></i> Fluid grid system</a></li>
                          <li><a href="#layouts"><i class="icon-chevron-right"></i> Layouts</a></li>
                          <li><a href="#responsive"><i class="icon-chevron-right"></i> Responsive design</a></li>
                        </ul>  
                    </nav>
                </div>
            </div>
        </div>

        <footer>
            <p class="text-center">Prepared by Keith Wickramasekara. 2014</p>
        </footer>
        

        <!-- jQuery -->
        <script src="js/jquery.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>

        <!-- Plugin JavaScript -->
        <script src="js/jquery.easing.min.js"></script>

        <!-- Custom Theme JavaScript -->
        <script src="js/grayscale.js"></script>
        <script src="js/modal.js"></script>
            </body>
</html>