-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 19 Apr 2016 pada 08.17
-- Versi Server: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `absen`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_absen`
--

CREATE TABLE IF NOT EXISTS `data_absen` (
`id_absen` int(15) NOT NULL,
  `id_karyawan` int(15) NOT NULL,
  `nama_lengkap` varchar(35) NOT NULL,
  `divisi` varchar(25) NOT NULL,
  `tanggal_absen` date NOT NULL,
  `keterangan` varchar(10) NOT NULL,
  `keterangan_lengkap` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_karyawan`
--

CREATE TABLE IF NOT EXISTS `data_karyawan` (
  `id_karyawan` int(15) NOT NULL,
  `nama_lengkap` varchar(35) NOT NULL,
  `divisi` varchar(25) NOT NULL,
  `alamat` varchar(150) NOT NULL,
  `jenis_kelamin` varchar(15) NOT NULL,
  `no_telepon` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `log_absen_keluar`
--

CREATE TABLE IF NOT EXISTS `log_absen_keluar` (
`id_log_keluar` int(11) NOT NULL,
  `id_karyawan` int(15) NOT NULL,
  `tanggal_pulang` date NOT NULL,
  `jam_keluar` varchar(10) NOT NULL,
  `keterangan` varchar(100) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data untuk tabel `log_absen_keluar`
--

INSERT INTO `log_absen_keluar` (`id_log_keluar`, `id_karyawan`, `tanggal_pulang`, `jam_keluar`, `keterangan`) VALUES
(1, 13, '2016-04-19', '09:59:14', ''),
(2, 56, '2016-04-19', '10:07:32', ''),
(3, 90, '2016-04-19', '10:33:35', ''),
(4, 56, '2016-04-19', '10:45:09', ''),
(5, 78, '2016-04-19', '11:22:49', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `log_absen_masuk`
--

CREATE TABLE IF NOT EXISTS `log_absen_masuk` (
`id_log_masuk` int(10) NOT NULL,
  `id_karyawan` varchar(15) NOT NULL,
  `tanggal_masuk` date NOT NULL,
  `jam_masuk` varchar(15) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data untuk tabel `log_absen_masuk`
--

INSERT INTO `log_absen_masuk` (`id_log_masuk`, `id_karyawan`, `tanggal_masuk`, `jam_masuk`) VALUES
(1, '33', '2016-04-04', '5544556'),
(2, '43', '2016-04-18', '14:27:40'),
(3, '43', '2016-04-18', '14:29:18'),
(4, '43', '2016-04-18', '14:30:58'),
(5, '13', '2016-04-18', '14:33:01'),
(6, '45', '2016-04-18', '14:35:50'),
(7, '222', '2016-04-18', '15:11:36'),
(8, '13', '2016-04-18', '15:18:05'),
(9, '776', '2016-04-18', '15:41:04'),
(10, '10', '2016-04-18', '15:41:35'),
(11, '12', '2016-04-19', '09:07:21'),
(12, '14', '2016-04-19', '09:15:38'),
(13, '15', '2016-04-19', '09:19:16'),
(14, '11', '2016-04-19', '09:23:51'),
(15, '16', '2016-04-19', '09:26:50'),
(16, '17', '2016-04-19', '09:27:38'),
(17, '18', '2016-04-19', '09:27:46');

-- --------------------------------------------------------

--
-- Struktur dari tabel `log_data_lembur`
--

CREATE TABLE IF NOT EXISTS `log_data_lembur` (
`id_data_lembur` int(15) NOT NULL,
  `id_karyawan` int(15) NOT NULL,
  `nama_lengkap` varchar(35) NOT NULL,
  `divisi` varchar(25) NOT NULL,
  `tanggal_lembur` date NOT NULL,
  `jam_lembur` varchar(15) NOT NULL,
  `keterangan` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `data_absen`
--
ALTER TABLE `data_absen`
 ADD PRIMARY KEY (`id_absen`);

--
-- Indexes for table `data_karyawan`
--
ALTER TABLE `data_karyawan`
 ADD PRIMARY KEY (`id_karyawan`);

--
-- Indexes for table `log_absen_keluar`
--
ALTER TABLE `log_absen_keluar`
 ADD PRIMARY KEY (`id_log_keluar`);

--
-- Indexes for table `log_absen_masuk`
--
ALTER TABLE `log_absen_masuk`
 ADD PRIMARY KEY (`id_log_masuk`), ADD UNIQUE KEY `id_log_masuk` (`id_log_masuk`), ADD UNIQUE KEY `id_log_masuk_2` (`id_log_masuk`);

--
-- Indexes for table `log_data_lembur`
--
ALTER TABLE `log_data_lembur`
 ADD PRIMARY KEY (`id_data_lembur`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `data_absen`
--
ALTER TABLE `data_absen`
MODIFY `id_absen` int(15) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `log_absen_keluar`
--
ALTER TABLE `log_absen_keluar`
MODIFY `id_log_keluar` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `log_absen_masuk`
--
ALTER TABLE `log_absen_masuk`
MODIFY `id_log_masuk` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `log_data_lembur`
--
ALTER TABLE `log_data_lembur`
MODIFY `id_data_lembur` int(15) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
