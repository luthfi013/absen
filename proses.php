<?php
// process.php

$errors         = array();      // array to hold validation errors
$data           = array();      // array to pass back data

// validate the variables ======================================================
    // if any of these variables don't exist, add an error to our $errors array

    if (empty($_POST['no_id']))
        $errors['no_id'] = 'no_id tidak boleh kosong.';

// return a response ===========================================================

    // if there are any errors in our errors array, return a success boolean of false
    if ( ! empty($errors)) {
        $data['deskripsi'] = "error/gagal";
        $data['errors']  = $errors;
    } else {
        $hasil = insertID($data);
        // bikin kondisi
        if ($hasil['code'] == 00) { // mun sukses
             $data['message'] = $hasil['desc'];
             $data['nama'] = $hasil['nama'];
        }
        else {
            // didie message popup nu rek ditampilkeun mun gagal
             $data['message'] = $hasil['desc'];
             $data['nama'] = $hasil['nama'];
        }
       
    }

    // return all our data to an AJAX call
    echo json_encode($data);