<?php
include "config.php";

/* Attempt MySQL server connection. Assuming you are running MySQL
server with default setting (user 'root' with no password) */
$link = mysqli_connect("localhost", "root", "", "absen");
 
// Check connection
if($link === false){
    die("ERROR: Could not connect. " . mysqli_connect_error());
}
 
// Escape user inputs for security
$id_karyawan = mysqli_real_escape_string($link, $_POST['id_karyawan']);
$date = date('Y-m-d');
 
// attempt insert query execution
$sql = "INSERT INTO log_absen_masuk (id_karyawan, tanggal_masuk, jam_masuk) VALUES ($id_karyawan, '$date', CURTIME())";
if(mysqli_query($link, $sql)){
    header("Refresh:0; url=index_absen_masuk.html");
} else{
    header('Location: error_koneksi.php') . mysqli_error($link);
}
 
// close connection
mysqli_close($link);
?>