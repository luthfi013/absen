// magic.js
$(document).ready(function() {

    // process the form
    $('form').submit(function(event) {

        // get the form data / nyokot data ti form
        // there are many ways to get this data using jQuery (you can use the class or id also)
        var formData = {
            'no_id'              : $('input[name=no_id]').val()
        };

        // process the form
        $.ajax({
            type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
            url         : 'proses.php', // the url where we want to POST
            data        : formData, // our data object
            dataType    : 'json', // what type of data do we expect back from the server
                        encode          : true
        })
            // using the done promise callback
            .done(function(data) {

                alert(JSON.stringify(data));
                

                // here we will handle errors and validation messages
            });

        // stop the form from submitting the normal way and refreshing the page
        event.preventDefault();
    });
});